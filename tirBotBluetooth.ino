#define X_DEADZONE 30
#define Y_DEADZONE 30
#define L293 1
constexpr uint8_t speedRight = 3,
                  speedLeft = 11,
                  dirRightA = 12,
                  dirLeftA = 13,
                  dirRightB = 9,
                  dirLeftB = 8,
                  currentSenseA = A0,
                  currentSenseB = A1;
void setup() {
#ifdef L293
  uint8_t outputPins[] = {speedRight, speedLeft, dirRightA, dirLeftA, dirRightB, dirLeftB};
#endif
#ifdef L298
  uint8_t outputPins[] = {speedRight, speedLeft, dirRight, dirLeft, brakeRight, brakeLeft};
#endif
  for (int i = 0; i < sizeof(outputPins) / sizeof(outputPins[0]); i++) {
    pinMode(outputPins[i], OUTPUT);
    digitalWrite(outputPins[i], LOW);
  }
  Serial.begin(9600);
}
void loop() {
  if (Serial.available()) {
    char c = Serial.read();
    switch (c) {
      case 'X': {
          int x = Serial.parseInt();
          Serial.read();
          int y = Serial.parseInt();
          Serial.read();
          setMotors(x, y);
          break;
        }
      case 'L':
        test();
        Serial.read();
      default:
        break;
    }
  }
}
void setMotors(int16_t x, int16_t y) {
  bool forwardLeft, forwardRight;
  uint8_t speed;
  uint8_t speedLeftValue, speedRightValue;
  speed = map(abs(y), 0, 100, 0, 255);
  forwardLeft = forwardRight = (y > 0) ? true : false;
  if (speed < Y_DEADZONE) speed = 0;
  speedLeftValue = speedRightValue = speed;
  if (x < -X_DEADZONE) {
    forwardLeft = !forwardLeft;
    speedLeftValue *= -x / 200.0;
  }
  if (x > X_DEADZONE) {
    forwardRight = !forwardRight;
    speedRightValue *= x / 200.0;
  }
#ifdef L293
  digitalWrite(dirLeftA, forwardLeft);
  digitalWrite(dirLeftB, !forwardLeft);
  digitalWrite(dirRightA, forwardRight);
  digitalWrite(dirRightB, !forwardRight);
#endif
#ifdef L298
  digitalWrite(dirLeft, forwardLeft);
  digitalWrite(dirRight, forwardRight);
#endif
  analogWrite(speedLeft, speedLeftValue);
  analogWrite(speedRight, speedRightValue);
}
void test() {
  Serial.print("*DAcceleration\n*");
  bool forwardLeft = true, forwardRight = true;
  digitalWrite(dirLeftA, forwardLeft);
  digitalWrite(dirLeftB, !forwardLeft);
  digitalWrite(dirRightA, forwardRight);
  digitalWrite(dirRightB, !forwardRight);
  for (int i = 0; i < 256; i++) {
    analogWrite(speedLeft, i);
    analogWrite(speedRight, i);
    delay(30);
  }
  Serial.print("*DAcceleration backwards\n*");
  forwardLeft = forwardRight = false;
  digitalWrite(dirLeftA, forwardLeft);
  digitalWrite(dirLeftB, !forwardLeft);
  digitalWrite(dirRightA, forwardRight);
  digitalWrite(dirRightB, !forwardRight);
  for (int i = 0; i < 256; i++) {
    analogWrite(speedLeft, i);
    analogWrite(speedRight, i);
    delay(30);
  }
  Serial.print("*DAcceleration right\n*");
  forwardLeft = false;
  forwardRight = true;;
  digitalWrite(dirLeftA, !forwardLeft);
  digitalWrite(dirLeftB, !forwardLeft);
  digitalWrite(dirRightA, forwardRight);
  digitalWrite(dirRightB, !forwardRight);
  for (int i = 0; i < 256; i++) {
    analogWrite(speedLeft, i);
    analogWrite(speedRight, i);
    delay(30);
  }
  Serial.print("*DAcceleration left\n*");
  forwardLeft = true;
  forwardRight = false;
  digitalWrite(dirLeftA, forwardLeft);
  digitalWrite(dirLeftB, !forwardLeft);
  digitalWrite(dirRightA, !forwardRight);
  digitalWrite(dirRightB, !forwardRight);
  for (int i = 0; i < 256; i++) {
    analogWrite(speedLeft, i);
    analogWrite(speedRight, i);
    delay(30);
  }
  Serial.print("*DStop\n*");
  digitalWrite(dirLeftA, true);
  digitalWrite(dirLeftB, true);
  digitalWrite(dirRightA, true);
  digitalWrite(dirRightB, true);
  analogWrite(speedLeft, 0);
  analogWrite(speedRight, 0);
}
